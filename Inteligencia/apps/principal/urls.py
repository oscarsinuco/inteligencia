from django.urls import path
from . import views
app_name = 'principal'
urlpatterns = [
    # post views
    path('', views.principal, name='principal')
]