from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from .functions.obtener_latitud_longitud import obtener_latitud_longitud
import warnings
warnings.filterwarnings("ignore")
def personas(request):
    return render(request,'index_personas.html')
def calcular_ruta_personas(request):
	dia = int(request.GET.get("dia", None))
	mes = int(request.GET.get("mes", None))
	genero = int(request.GET.get("genero", None))
	metodo = int(request.GET.get("metodo", None))
	edad = int(request.GET.get("edad", None))
	data = obtener_latitud_longitud(dia, mes, genero, metodo, edad)
	return JsonResponse(data)