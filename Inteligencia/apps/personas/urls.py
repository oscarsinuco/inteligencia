from django.urls import path
from . import views
app_name = 'personas'
urlpatterns = [
    # post views
    path('', views.personas, name='personas'),
    path('calcular_ruta_personas/', views.calcular_ruta_personas, name='calcular_zona_critica')
]