import numpy as np
import tensorflow as tf
from sklearn.externals import joblib
def obtener_latitud_longitud(dia, mes, agresor, delito, arma):
    dia_prueba = dia
    mes_prueba = mes
    agresor_prueba = agresor
    delito_prueba = delito
    arma_prueba = arma
    com=[]
    def delcomunas():
        nombre_comunas=['cabecera_del_llano','centro','garcia_rovira','la_ciudadela','la_concordia','la_pedregosa','lagos_del_cacique','morrorico','mutis','nor_oriental','norte','occidental','oriental','provenza','san_francisco','sur','sur_occidente']
        por = 0
        for i in range(len(nombre_comunas)):
            est = tf.keras.models.load_model(nombre_comunas[i]+'_pol',custom_objects=None,compile=False)
            test=np.array([[dia_prueba, mes_prueba, agresor_prueba, delito_prueba, arma_prueba]]).reshape(1,5)
            latitud = est.predict(test)[0][0]
            longitud = est.predict(test)[0][1]
            por = por + float(1/len(nombre_comunas))
            print(str(int(float(100*por))) + "%")
            radio=0.001
            com.append([float(latitud),float(longitud),radio])
        cont=0    
        data={}
        for k in nombre_comunas:
            data[k]=com[cont]
            cont=cont+1
        return data
    data = delcomunas()
    return data