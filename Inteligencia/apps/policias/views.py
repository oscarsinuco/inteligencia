from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from .functions.obtener_latitud_longitud import obtener_latitud_longitud
def policias(request):
    return render(request,'index_policias.html')
def calcular_zona_critica(request):
	dia = int(request.GET.get("dia", None))
	mes = int(request.GET.get("mes", None))
	agresor = int(request.GET.get("agresor", None))
	delito = int(request.GET.get("delito", None))
	arma = int(request.GET.get("arma", None))
	data = obtener_latitud_longitud(dia, mes, agresor, delito, arma)
	return JsonResponse(data)