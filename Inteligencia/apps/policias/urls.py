from django.urls import path
from . import views
app_name = 'policias'
urlpatterns = [
    # post views
    path('', views.policias, name='policias'),
    path('calcular_zona_critica/', views.calcular_zona_critica, name='calcular_zona_critica')
]