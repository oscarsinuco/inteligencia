from django.apps import AppConfig


class PoliciasConfig(AppConfig):
    name = 'policias'
