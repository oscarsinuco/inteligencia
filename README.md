Un proyecto para predecir robos en alguna ciudad, en este caso la ciudad de BUCARAMANGA - COLOMBIA

Keywords: Inteligencia, Artificial, Learning, Python, Django, TensorFlow



Intrucciones:

- El notebook donde se hizo el debido tratamiento de datos se encuentra en la página de inicio del repositorio y se llama proyecto_check_your_zone
- Para ejecutar la aplicación correctamente en sus computadores se necesitan cumplir las siguientes condiciones:

    1. Instalar python (Versión 3.5.0) junto con pip
    2. Instalar Framework Django (Versión 2.2.3)
    3. Instalar Tensorflow y Numpy mediante pip
    4. Tener conexión a internet
    5. Abrir una consola y navegar al directorio del proyecto descargado
    6. Escribir en consola: cd Inteligencia
    7. Ejecutar el comando: python manage.py runserver
    8. El proyecto funciona correctamente si se obtiene la siguiente linea en la consola: Starting development server at http://127.0.0.1:8000/
    9. Acceder mediante un navegador (Chrome es recomendado) a la dirección http://127.0.0.1:8000/
    10. Disfruta de nuestra app
    